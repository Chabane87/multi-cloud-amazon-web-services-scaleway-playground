# Amazon Web Services

Export global variables

```shell
export AWS_PROFILE=<MY_PROFILE>
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)
export SW_PROJECT_NAME=<SW_PROJECT_NAME>
export DEMO_ENV_GIT_REPOSITORY_URL=<MY_REPO>/demo-env.git
export AWS_REGION=eu-west-1

export EKS_CLUSTER_NAME="devops"

export R53_HOSTED_ZONE_ID=<R53_HOSTED_ZONE_ID>
export ACM_VAULT_ARN=<ACM_VAULT_ARN>
export ACM_ARGOCD_ARN=<ACM_ARGOCD_ARN>
export PUBLIC_DNS_NAME=<PUBLIC_DNS_NAME>

export TERRAFORM_BUCKET_NAME=bucket-${AWS_ACCOUNT_ID}-sw-aws-terraform-backend
```

Create s3 bucket for terraform states

```shell
# Create bucket
aws s3api create-bucket \
     --bucket $TERRAFORM_BUCKET_NAME \
     --region $AWS_REGION \
     --create-bucket-configuration LocationConstraint=$AWS_REGION

# Make it not public     
aws s3api put-public-access-block \
    --bucket $TERRAFORM_BUCKET_NAME \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

# Enable versioning
aws s3api put-bucket-versioning \
    --bucket $TERRAFORM_BUCKET_NAME \
    --versioning-configuration Status=Enabled
```

Initialize AWS devops infrastructure. The states will be saved in AWS.

```shell
cd infra/plan
terraform init \
    -backend-config="bucket=$TERRAFORM_BUCKET_NAME" \
    -backend-config="key=devops/terraform-state" \
    -backend-config="region=$AWS_REGION"
```

Complete `infra/plan/terraform.tfvars` and run 

```shell
sed -i "s/<LOCAL_IP_RANGES>/$(curl -s http://checkip.amazonaws.com/)\/32/g; s/<PUBLIC_DNS_NAME>/${PUBLIC_DNS_NAME}/g; s/<AWS_ACCOUNT_ID>/${AWS_ACCOUNT_ID}/g; s/<AWS_REGION>/${AWS_REGION}/g; s/<EKS_CLUSTER_NAME>/${EKS_CLUSTER_NAME}/g; s,<ACM_VAULT_ARN>,${ACM_VAULT_ARN},g;" terraform.tfvars
terraform apply
```

This will create the following resources:

- Enables the required services on that project
- Creates a bucket for storage
- Creates a KMS key for encryption
- Creates an IAM Role with the most restrictive permissions to those resources
- Creates a EKS cluster with the configured IAM Role attached
- Creates an Elastic IP
- Configures Terraform to talk to Kubernetes
- Creates a Kubernetes secret with the TLS file contents
- Configures your local system to talk to the EKS cluster by getting the cluster credentials and kubernetes context
- Submits the StatefulSet and Service to the Kubernetes API

Access the EKS Cluster using

```shell
aws eks --region $AWS_REGION update-kubeconfig --name $EKS_CLUSTER_NAME
kubectl config set-context --current --namespace=vault-server
```

# Vault

Install [vault](https://learn.hashicorp.com/tutorials/vault/getting-started-install) 

Vault reads these environment variables for communication. Set Vault's address, and the initial root token.

```shell
# Make sure you are in the terraform/ directory
# cd infra/plan

export VAULT_ADDR="https://vault.${PUBLIC_DNS_NAME}"
export VAULT_TOKEN="$(aws secretsmanager get-secret-value --secret-id $(terraform output vault_secret_name) --version-stage AWSCURRENT --query SecretString --output text | grep "Initial Root Token: " | awk -F ': ' '{print $2}')"
```

Save scaleway credentials:

```shell
SCW_ACCESS_KEY=<SCW_ACCESS_KEY>
SCW_SECRET_KEY=<SCW_SECRET_KEY>
SCW_DEFAULT_PROJECT_ID=<SCW_DEFAULT_PROJECT_ID>
vault secrets enable -path=scaleway/projects/${SW_PROJECT_NAME} -version=2 kv
vault kv put scaleway/projects/${SW_PROJECT_NAME}/credentials/access key="$SCW_ACCESS_KEY"
vault kv put scaleway/projects/${SW_PROJECT_NAME}/credentials/secret key="$SCW_SECRET_KEY"
vault kv put scaleway/projects/${SW_PROJECT_NAME}/config id="$SCW_DEFAULT_PROJECT_ID"
```

Create IAM role to authorize GitLab to read from terraform state

```shell
ISSUER_HOSTPATH=$(terraform output eks_issuer_url)

PROVIDER_ARN="arn:aws:iam::$AWS_ACCOUNT_ID:oidc-provider/$ISSUER_HOSTPATH"

cat > trust-policy.json << EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "$PROVIDER_ARN"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${ISSUER_HOSTPATH}:sub": "system:serviceaccount:sw-dev:app-deployer",
          "${ISSUER_HOSTPATH}:aud": "sts.amazonaws.com"
        }
      }
    }
  ]
}
EOF

SW_ROLE_NAME=sw-deployer-role

aws iam create-role \
          --role-name $SW_ROLE_NAME  \
          --assume-role-policy-document file://trust-policy.json

SW_DEPLOYER_ROLE_ARN=$(aws iam get-role \
                        --role-name $SW_ROLE_NAME \
                        --query Role.Arn --output text)
```

```shell
kubectl create namespace sw-dev
```

To bind an IAM role to a ksa

```shell
kubectl create serviceaccount -n sw-dev app-deployer

cat > sw-policy.json << EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "SecretManagerReadSecrets",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetSecretValue"
            ],
            "Resource": [
                "arn:aws:secretsmanager:$AWS_REGION:$AWS_ACCOUNT_ID:secret:vault-token*",
                "arn:aws:secretsmanager:$AWS_REGION:$AWS_ACCOUNT_ID:secret:gitlab-ssh-key*",
                "arn:aws:secretsmanager:$AWS_REGION:$AWS_ACCOUNT_ID:secret:argocd-token*"
            ]
        },
        {
            "Sid": "S3BucketTerraformStatesPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::bucket-$AWS_ACCOUNT_ID-sw-aws-terraform-backend"
            ]
        },
        {
            "Sid": "S3ObjectTerraformStatesPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::bucket-$AWS_ACCOUNT_ID-sw-aws-terraform-backend/scaleway/*"
            ]
        },
        {
            "Sid": "S3BucketCodeBuildPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::codebuild-$AWS_REGION-$AWS_ACCOUNT_ID-input-bucket"
            ]
        },
        {
            "Sid": "S3ObjectCodeBuildPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::codebuild-$AWS_REGION-$AWS_ACCOUNT_ID-input-bucket/demo/*"
            ]
        },
        {
            "Sid": "CodeBuildStartPolicy",
            "Effect": "Allow",
            "Action": [
                "codebuild:StartBuild",
                "codebuild:BatchGet*"
            ],
            "Resource": [
                "arn:aws:codebuild:$AWS_REGION:$AWS_ACCOUNT_ID:project/build-demo-docker-image"
            ]
        },
        {
            "Sid": "ECRAccessPolicy",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "LogsAccessPolicy",
            "Effect": "Allow",
            "Action": [
                "logs:FilterLogEvents"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "EKSAccessPolicy",
            "Effect": "Allow",
            "Action": [
                "eks:DescribeCluster"
            ],
            "Resource": [
                "arn:aws:eks:$AWS_REGION:$AWS_ACCOUNT_ID:cluster/$EKS_CLUSTER_NAME"
            ]
        },
        {
            "Sid": "ECRPullPolicy",
            "Effect": "Allow",
            "Action": [
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage"
            ],
            "Resource": [
                "arn:aws:ecr:$AWS_REGION:$AWS_ACCOUNT_ID:repository/demo"
            ]
        },
        {
            "Sid": "LogsAccessPolicy",
            "Effect": "Allow",
            "Action": [
                "logs:GetLogEvents"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF

aws iam put-role-policy \
        --role-name $SW_ROLE_NAME \
        --policy-name sw-policy \
        --policy-document file://sw-policy.json

kubectl annotate serviceAccount app-deployer -n sw-dev eks.amazonaws.com/role-arn=$SW_DEPLOYER_ROLE_ARN
```

Create the policy named policy-sw-dev-deployer with the contents from stdin

```shell
vault policy write policy-sw-dev-deployer - <<EOF
# Read-only permissions

path "scaleway/projects/${SW_PROJECT_NAME}/*" {
  capabilities = [ "read" ]
}

EOF
```

Create a token and add the policy-sw-dev-deployer policy.

```shell
GITLAB_RUNNER_VAULT_TOKEN=$(vault token create -policy=policy-sw-dev-deployer | grep "token" | awk 'NR==1{print $2}')
```

To get a temporary access token, configure auth methods to automatically assign a set of policies to tokens

```shell
vault auth enable approle

vault write auth/approle/role/sw-dev-deployer \
    secret_id_ttl=10m \
    token_num_uses=10 \
    token_ttl=20m \
    token_max_ttl=30m \
    secret_id_num_uses=40 \
    token_policies=policy-sw-dev-deployer

ROLE_ID=$(vault read -field=role_id auth/approle/role/sw-dev-deployer/role-id)
SECRET_ID=$(vault write -f -field=secret_id auth/approle/role/sw-dev-deployer/secret-id)
GITLAB_RUNNER_VAULT_TOKEN=$(vault write auth/approle/login role_id="$ROLE_ID" secret_id="$SECRET_ID" | grep "token" | awk 'NR==1{print $2}')
```

Save vault token in secret manager

```shell
# Create
aws secretsmanager create-secret --name vault-token \
    --description "Vault Token" \
    --secret-string "${GITLAB_RUNNER_VAULT_TOKEN}"

# Update
aws secretsmanager update-secret --secret-id vault-token \
    --description "Vault Token" \
    --secret-string "${GITLAB_RUNNER_VAULT_TOKEN}"
```

# GitLab CI

Add Gitlab SSH Key to allow the runner to push on git repositories

```shell
cd ~/.ssh
ssh-keygen -t rsa -b 4096
aws secretsmanager create-secret --name gitlab-ssh-key \
    --description "Gitlab SSH Key" \
    --secret-string file://id_rsa
```

Create service role for Code Build

```shell
cat > trust-policy.json << EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

CODEBUILD_ROLE_NAME="devops-code-build-service-role"

aws iam create-role \
          --role-name $CODEBUILD_ROLE_NAME  \
          --assume-role-policy-document file://trust-policy.json

ROLE_ARN=$(aws iam get-role \
                        --role-name $CODEBUILD_ROLE_NAME \
                        --query Role.Arn --output text)

cat > code-build-policy.json << EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CloudWatchLogsPolicy",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "SecretManagerReadSecrets",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetSecretValue"
            ],
            "Resource": [
                "arn:aws:secretsmanager:$AWS_REGION:$AWS_ACCOUNT_ID:secret:gitlab-ssh-key*"
            ]
        },
        {
            "Sid": "S3GetObjectPolicy",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": [
                "arn:aws:s3:::codebuild-$AWS_REGION-$AWS_ACCOUNT_ID-input-bucket/*"
            ]
        },
        {
            "Sid": "ECRPullPolicy",
            "Effect": "Allow",
            "Action": [
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage"
            ],
            "Resource": [
                "arn:aws:ecr:$AWS_REGION:$AWS_ACCOUNT_ID:repository/tools",
                "arn:aws:ecr:$AWS_REGION:$AWS_ACCOUNT_ID:repository/awscli"
            ]
        },
        {
            "Sid": "ECRAuthPolicy",
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "ECRPushPolicy",
            "Effect": "Allow",
            "Action": [
                "ecr:CompleteLayerUpload",
                "ecr:InitiateLayerUpload",
                "ecr:PutImage",
                "ecr:UploadLayerPart"
            ],
            "Resource": [
                "arn:aws:ecr:$AWS_REGION:$AWS_ACCOUNT_ID:repository/tools"
            ]
        },
        {
            "Sid": "S3BucketIdentity",
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketAcl",
                "s3:GetBucketLocation"
            ],
            "Resource": "arn:aws:s3:::codebuild-$AWS_REGION-$AWS_ACCOUNT_ID-input-bucket"
        }
    ]
}
EOF

aws iam put-role-policy \
        --role-name $CODEBUILD_ROLE_NAME \
        --policy-name devops-code-build-policy \
        --policy-document file://code-build-policy.json
```

Because docker has throttling for pulling the images, we will copy the `awscli` and `node` images on Amazon ECR

```shell
cd docker/awscli
docker build -t awscli:latest .
aws ecr create-repository --repository-name awscli --image-scanning-configuration scanOnPush=true --region $AWS_REGION &
docker tag awscli:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/awscli:latest
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/awscli:latest

cd docker/node
docker build -t node:latest .
aws ecr create-repository --repository-name node --image-scanning-configuration scanOnPush=true --region $AWS_REGION &
docker tag node:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/node:latest
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/node:latest
```

Create docker image with kubectl, kustomize, vault, argocd, terraform, aws cli, sw cli. It will be used by the gitlab runner

```shell
KUBECTL_VERSION=1.20.5
KUSTOMIZE_VERSION=4.0.5
ARGOCD_VERSION=1.7.4
VAULT_VERSION=1.6.0
TERRAFORM_VERSION=0.13.6
SCW_VERSION=2.3.0

cd docker/tools
sed -i "s/<AWS_REGION>/$AWS_REGION/g; s/<AWS_ACCOUNT_ID>/$AWS_ACCOUNT_ID/g;" Dockerfile

# Initiliaze CodeBuild project
IMAGE_TAG="latest"
IMAGE_REPO_NAME="tools"
sed -i "s/<IMAGE_REPO_NAME>/$IMAGE_REPO_NAME/g; s/<IMAGE_TAG>/$IMAGE_TAG/g; s/<AWS_DEFAULT_REGION>/$AWS_REGION/g; s/<AWS_ACCOUNT_ID>/$AWS_ACCOUNT_ID/g; s,<ROLE_ARN>,$ROLE_ARN,g;" project.json

# Create bucket to store artifacts
CODEBUILD_BUCKET=codebuild-$AWS_REGION-$AWS_ACCOUNT_ID-input-bucket

aws s3api create-bucket \
     --bucket $CODEBUILD_BUCKET \
     --region $AWS_REGION \
     --create-bucket-configuration LocationConstraint=$AWS_REGION

aws s3api put-public-access-block \
    --bucket $CODEBUILD_BUCKET \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

# Create ECR Repository
aws ecr create-repository --repository-name $IMAGE_REPO_NAME

# Prepare new build
sed -i "s/<KUBECTL_VERSION>/$KUBECTL_VERSION/g;s/<KUSTOMIZE_VERSION>/$KUSTOMIZE_VERSION/g;s/<ARGOCD_VERSION>/$ARGOCD_VERSION/g;s/<VAULT_VERSION>/$VAULT_VERSION/g;s/<TERRAFORM_VERSION>/$TERRAFORM_VERSION/g;s/<SCW_VERSION>/$SCW_VERSION/g;s/<IMAGE_TAG>/$IMAGE_TAG/g;" build.json

zip -r tools.zip . -x '*.json'
aws s3api put-object --bucket $CODEBUILD_BUCKET --key devops/tools/$IMAGE_TAG/image.zip --body tools.zip

# Run project
aws codebuild create-project --cli-input-json file://project.json &

# Run build
aws codebuild start-build \
    --project-name "build-tools-docker-image" \
    --cli-input-json file://build.json &
```

Install Helm 3

```shell
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

Add the kubernetes role to the Kubernetes service account that will be used by Gitlab CI

```shell
kubectl apply -f gitlab/dev/rbac-gitlab-demo-dev.yml
```

Add gitlab package

```shell
helm repo add gitlab https://charts.gitlab.io
```

Save the runner registration token in a secret

```shell
DEMO_INFRA_REPO_RUNNER_TOKEN=<DEMO_INFRA_REPO_RUNNER_TOKEN>
kubectl create secret generic secret-sw-devops-gitlab-runner-tokens --from-literal=runner-token='' --from-literal=runner-registration-token='$DEMO_INFRA_REPO_RUNNER_TOKEN' -n sw-dev
```

Clone the repository and install the Gitlab Runner

```shell
helm install -n sw-dev sw-dev -f gitlab/dev/values.yaml gitlab/gitlab-runner
```

Test if the gitlab runner authorization

```shell
kubectl run -it \
  --image ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/tools \
  --serviceaccount app-deployer \
  --namespace sw-dev \
  gitlab-runner-auth-test sts get-caller-identity

kubectl logs gitlab-runner-auth-test -n sw-dev
```

Inside the pod container, run `aws sts get-caller-identity`. You can delete the pod afterwards `kubectl delete pod gitlab-runner-auth-test -n sw-dev`.

# Gitlab Projects

- Add protected tags `v*` on both repositories `-app` and `-env`. Go to `Settings > Repository > Protected Tags`.
- Enable gitlab runners on `-infra`, `-env` and `-app`. Go to `Settings > CI / CD > Runners > Specific Runners > Enable for this project`.
- Lock gitlab runners for current projets (icon "edit" of the activated runner).

# ArgoCD

Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.

Install Argo CD

```shell
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Access the Argo CD API Server

```shell
cd infra/plan
sed -i "s,<ACM_ARGOCD_ARN>,${ACM_ARGOCD_ARN},g; s/<LOCAL_IP_RANGES>/$(curl -s http://checkip.amazonaws.com/)\/32/g; s/<NAT_IP_1_RANGES>/$(terraform output nat1_ip)\/32/g; s/<NAT_IP_2_RANGES>/$(terraform output nat2_ip)\/32/g; " ../../k8s/argocd-server.patch.yaml
cd k8s
kubectl patch svc argocd-server -n argocd -p "$(cat argocd-server.patch.yaml)"
```

Download Argo CD CLI

```shell
ARGOCD_VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$ARGOCD_VERSION/argocd-linux-amd64
chmod +x /usr/local/bin/argocd
```

Configure app-demo-dev application

```shell
cd k8s
export GITLAB_USERNAME_SECRET=<GITLAB_USERNAME_SECRET>
export GITLAB_CI_PUSH_TOKEN=<GITLAB_CI_PUSH_TOKEN>
kubectl create secret generic demo -n argocd \
--from-literal=username=$GITLAB_USERNAME_SECRET \
--from-literal=password=$GITLAB_CI_PUSH_TOKEN
```

If you have a Route 53 Hosted Zone available, we can create an ingress for ArgoCD and attach an Elastic IP

```shell
cat > argocd-recordset.json << EOF
{
            "Changes": [{
            "Action": "CREATE",
                        "ResourceRecordSet": {
                                    "Name": "argocd.${PUBLIC_DNS_NAME}.",
                                    "Type": "CNAME",
                                    "TTL": 300,
                                 "ResourceRecords": [{ "Value": "$(kubectl get services -n argocd argocd-server --output jsonpath='{.status.loadBalancer.ingress[0].hostname}')"}]
}}]
}
EOF

aws route53 change-resource-record-sets --hosted-zone-id $R53_HOSTED_ZONE_ID --change-batch file://argocd-recordset.json
```

Enable argocd

```shell
kubectl patch deployment argocd-server -n argocd -p "$(cat argocd-deployment-server.patch.yaml)" 
```

Login using the CLI

```shell
ARGOCD_ADDR="argocd.${PUBLIC_DNS_NAME}"
# get default password
ARGOCD_DEFAULT_PASSWORD=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)
argocd login $ARGOCD_ADDR
# change password
argocd account update-password
# for any issue, reset the password, edit the argocd-secret secret and update the admin.password field with a new bcrypt hash. You can use a site like https://www.browserling.com/tools/bcrypt to generate a new hash.
kubectl -n argocd patch secret argocd-secret \
  -p '{"stringData": {
    "admin.password": "<BCRYPT_HASH>",
    "admin.passwordMtime": "'$(date +%FT%T%Z)'"
  }}'
```

Create repositories, users and rbacs

```shell
sed -i "s,<GIT_REPOSITORY_URL>,$DEMO_ENV_GIT_REPOSITORY_URL,g" argocd-configmap.yaml
kubectl apply -n argocd -f argocd-configmap.yaml
kubectl apply -n argocd -f argocd-rbac-configmap.yaml
```

Change demo user password

```shell
argocd account update-password --account demo --current-password "${ARGOCD_DEFAULT_PASSWORD}" --new-password "<NEW_PASSWORD>"
```

Generate an access token for argocd

```shell
AROGOCD_TOKEN=$(argocd account generate-token --account gitlab)
```

Save argocd token in secret manager

```shell
# Create
aws secretsmanager create-secret --name argocd-token \
    --description "ArgoCD Token" \
    --secret-string "${AROGOCD_TOKEN}"

# Update
aws secretsmanager update-secret --secret-id argocd-token \
    --description "ArgoCD Token" \
    --secret-string "${AROGOCD_TOKEN}"
```

Add autorisations to the runner to apply changes on ArgoCD namespace 

```shell
cat - <<EOF | kubectl apply -f - --namespace "argocd"
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: $SW_ROLE_NAME
  namespace: argocd
rules:
  - apiGroups: ["argoproj.io"]
    resources: ["clusters", "projects", "applications", "repositories", "certificates", "accounts", "gpgkeys"]
    verbs: ["get", "create", "update", "delete", "sync", "override", "action"]
EOF    

cat - <<EOF | kubectl apply -f - --namespace "argocd"
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: $SW_ROLE_NAME
  namespace: argocd
subjects:
- kind: User
  name: $SW_ROLE_NAME
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: $SW_ROLE_NAME
  apiGroup: rbac.authorization.k8s.io
EOF

kubectl get configmap -n kube-system aws-auth -o yaml > aws-auth.yaml

# complete the manifest with the following
  mapRoles: | 
    - rolearn: $SW_DEPLOYER_ROLE_ARN
      username: $SW_ROLE_NAME

kubectl apply -f aws-auth.yaml
```

# Scaleway

Install [sw-cli](https://github.com/scaleway/scaleway-cli)

```shell
scw init
```

**Now you can run the gitlab ci pipeline ! With the following Gitlab CI/CD Variables**

In `demo-infra` repo

```shell
AWS_ACCOUNT_ID=$AWS_ACCOUNT_ID
AWS_REGION=$AWS_REGION
TERRAFORM_BUCKET_NAME=$TERRAFORM_BUCKET_NAME
SW_PROJECT_NAME=$SW_PROJECT_NAME
VAULT_ADDR=$VAULT_ADDR
ENV=dev
```

In `demo-env` repo

```shell
AWS_ACCOUNT_ID=$AWS_ACCOUNT_ID
AWS_REGION=$AWS_REGION
SW_PROJECT_NAME=$SW_PROJECT_NAME
ARGOCD_ADDR=$ARGOCD_ADDR
VAULT_ADDR=$VAULT_ADDR
ENV=dev
EKS_CLUSTER_NAME=$EKS_CLUSTER_NAME
```

In `demo-app` repo

```shell
AWS_ACCOUNT_ID=$AWS_ACCOUNT_ID
AWS_REGION=$AWS_REGION
CODEBUILD_BUCKET=$CODEBUILD_BUCKET
```

# Documentation

- [Authenticating and Reading Secrets With Hashicorp Vault](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)
- [How to setup Vault with Kubernetes](https://deepsource.io/blog/setup-vault-kubernetes/)
- [GitOps in Kubernetes: How to do it with GitLab CI and Argo CD](https://medium.com/@andrew.kaczynski/gitops-in-kubernetes-argo-cd-and-gitlab-ci-cd-5828c8eb34d6)
- [ArgoCD User Management](https://argoproj.github.io/argo-cd/operator-manual/user-management/)
- [How to configure and use AWS ECR with kubernetes & Rancher2.0](https://medium.com/@damitj07/how-to-configure-and-use-aws-ecr-with-kubernetes-rancher2-0-6144c626d42c)
- [Exposing Private ECR Images to External Users](https://www.derpturkey.com/exposing-private-ecr-images-to-external-users/)
- [Vault on Amazon EKS](https://aws-quickstart.github.io/quickstart-eks-hashicorp-vault/)
- [ArgoCD: an overview, SSL configuration, and an application deploy](https://itnext.io/argocd-an-overview-ssl-configuration-and-an-application-deploy-89d8947d95cf)
