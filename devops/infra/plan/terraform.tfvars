az                       = ["<AWS_REGION>a", "<AWS_REGION>b"]
region                   = "<AWS_REGION>"
acm_vault_arn            = "<ACM_VAULT_ARN>"
vpc_cidr_block           = "10.0.0.0/16"
public_dns_name          = "<PUBLIC_DNS_NAME>"
gitlab_public_ip_ranges  = "34.74.90.64/28"
authorized_source_ranges = "<LOCAL_IP_RANGES>"
