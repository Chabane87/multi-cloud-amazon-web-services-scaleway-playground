# Configuration

Share the specific runner `k8s-dev-runner` created in `devops` repo with this project.
You need `Maintainer` permission.

Complete the values

```shell
AWS_ACCOUNT_ID=<AWS_ACCOUNT_ID>
AWS_REGION=<AWS_REGION>
sed -i "s/<AWS_ACCOUNT_ID>/${AWS_ACCOUNT_ID}/g; s/<AWS_REGION>/${AWS_REGION}/g" base/demo-deployment.yaml
```

